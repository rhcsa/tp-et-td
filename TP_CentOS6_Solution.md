# Solution du TP CentOS 6 serveur web et sauvegarde.


## PRÉREQUIS : 

* laptop 192.168.10.1 *scp* student
* baremetal 192.168.1.100 *sshd*:22 user1
* vmcentos6 192.168.122.20 *sshd*;22 sshman
* *Shell Script*:  tar,date,find,rm
* Pas de consignes de sécurité : *iptables* et *selinux* désactivés.


## SOMMAIRE : 

[1.  On installe les outils nécessaires.](#part1)

[2.  Apache reverse proxy SSL.](#part2)

[3.  SFTP  Chroot/Jail.](#part3)

[4.  Test de nos services.](#part4)

[5.  Script de sauvegarde et vérification de la sauvegarde.](#part5)

[6.  Comment améliorer notre service ?](#part6)

<a name="part1">

### 1. On installe les outils nécessaires.

</a>


Nous nous connectons au système à administrer. 

Pour rappel nous partons d'un réseau pour aller en *ssh* sur le système installé *baremetal* et de là nous nous connectons en ssh vers la VM Centos 6.

```shell
student@laptop $ ssh user1@baremetal
user1@baremetal $ ssh sshman@vmcentos6
```


Voilà maintenant que nous sommes sur le système en question installons nos outils. 

En premier lieu activons les dépôts *epel* nécessaires pour *lighttpd*.

Sur la VM *centos6* :

```console
yum install -y epel-release
yum update
```

Ensuite nous installons nos outils: __apache__ , __mod_ssl__ pour le tls , __lighttpd__, __lftp__ et, pour nous faciliter nos configurations/logging, __tmux__.


```console
yum install -y httpd mod_ssl lighttpd lftp tmux 
```

Nous préparons le serveur *lighttpd* en le configurant dans `/etc/lighttpd.conf`.

* On désactive l'ipv6
* On force le localhost only
* On précise le port 8080
* On précise le root directory /srv/data

```bash
#######################################################################
##
## Some Variable définition which will make chrooting easier.
##
## if you add a variable here. Add the corresponding variable in the
## chroot example aswell.
##
var.log_root    = "/var/log/lighttpd"
var.server_root = "/srv/data"
var.state_dir   = "/var/run"
var.home_dir    = "/var/lib/lighttpd"
var.conf_dir    = "/etc/lighttpd"

#######################################################################
##
##  Basic Configuration
## ---------------------
##
server.port = 8080

##
## Use IPv6?
##
server.use-ipv6 = "disable"
                                                                                                                 
##                                                                                                               
## bind to a specific IP                                                                                         
##                                                                                                               
server.bind = "localhost"  

###############
##                                                                                                               
## Document root                                                                                                 
##                                                                                                               
server.document-root = server_root + "/srv/data"
```                                                                                                                 
 
```shell
service lighttpd start```

On vérifie que le service peut livrer le fichier *index.html* que nous avons au préalable mis en place avec les ACL adéquats.

```shell
ss -lapute | grep 8080
curl http://localhost/index.html
```

<a name="part2"> 
    
### 2.  Apache reverse proxy SSL.

</a>

Le principe est de rediriger tout ce qui passe par localhost:8080 vers localhost:443 via apache

![reverse-proxy-arch](/uploads/5d195c44781612908f69345a34729adb/reverse-proxy-arch.png)


Pour commencer vérifions la version de apache :

```shell
httpd -v

Server version: Apache/2.2.15 (Unix)
```

Oui c'est moche : 

![apache_outdated](/uploads/b0dbd4e121adbec9823afe414f48b827/apache_outdated.png)

Nous ne pourrons pas utiliser le TLS1.3 ni même le 1.2 car il faut une version égale ou supérieure à *2.4.38* . 

Malgré tout nous continuons.

Nous vérifions notre version d'openSSL

```shell
openssl version -v
OpenSSL 1.0.1e-fips 11 Feb 2013  
```


Oui je sais c'est vraiment moche. 

Nous allons utiliser le merveilleux outil de [mozilla générateur de certificats](https://ssl-config.mozilla.org/#server=apache&server-version=2.2.15&config=modern)

Nous y mettons nos version d'apache et openssl et vérifions la conf à appliquer.

Nous créons notre paire certificat/clé. 

```
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/httpd/ssl/httpd.key -out /etc/httpd/ssl/httpd.crt 
 ```

Nous vérifions que les modules nécessaires sont bien lancés avec *httpd* :

```shell
httpd -M | grep -E "ssl|proxy"  
```

```shell
 proxy_module (shared)
 proxy_balancer_module (shared)
 proxy_ftp_module (shared)
 proxy_http_module (shared)
 proxy_ajp_module (shared)
 proxy_connect_module (shared)
 ssl_module (shared)
```
Si ces modules ne sont pas lancés veillez à les de-commenter dans le fichier principal

```shell
LoadModule proxy_connect_module modules/mod_proxy_connect.so
LoadModule proxy_http_module modules/mod_proxy_http.so
LoadModule proxy_module modules/mod_proxy.so
LoadModule proxy_balancer_module modules/mod_proxy_balancer.so
LoadModule ssl_module modules/mod_ssl.so
```


Apache est une solution assez lourde et on peut facilement se perdre dans les fichiers de configuration.

Le fichier principal est `/etc/httpd/conf/httpd.conf` il régit le comportement global d'Apache. 

Les fichiers contenus dans `/etc/httpd/conf.d/` sont des fichiers supplémentaires et sont lus automatiquement 

Il faut que la directive : `Include conf.d/*.conf` soit déclarée dans le fichier principal.

Pour ce que nous voulons faire nous allons regrouper la conf dans `/etc/httpd/conf.d/reverse-proxy-ssl.conf` . Voici son contenu :

```perl

# SSL CONFIG
# Le lancement du module ssl se fait dans le fichier principal
# LoadModule ssl_module modules/mod_ssl.so

# Les paramètres du SSL 

ErrorLog logs/ssl_error_log
TransferLog logs/ssl_access_log

LogLevel warn
SSLCertificateFile /etc/httpd/ssl/httpd.crt
SSLCertificateKeyFile /etc/httpd/ssl/httpd.key
SSLProtocol             all -SSLv2 -SSLv3 -TLSv1 -TLSv1.1
SSLCipherSuite          ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384

# Notre paire cle cert 
SSLCertificateFile /etc/httpd/ssl/httpd.crt
SSLCertificateKeyFile /etc/httpd/ssl/httpd.key

CustomLog logs/ssl_request_log \
          "%t %h %{SSL_PROTOCOL}x %{SSL_CIPHER}x \"%r\" %b"
Listen 443

# Les reglages du virtual host qui redirige notre serveur local vers le  tout:443 .
<VirtualHost *:443>
SSLEngine on
        RedirectMatch ^/portal https://localhost
        ProxyPass / http://localhost:8080/
        ProxyPassReverse / http://localhost:8080/
</VirtualHost>

```

Nous démarrons notre service : 

```shell
service httpd start
```

Verifions nos socket et services :

```shell
ss -lapute | grep 8080
curl -kI https://localhost/portal
```


Le log avec : 

```shell
tail -f /var/log/httpd/*
```

<a name="part3">

### 3. SFTP  Chroot/Jail .

</a>

On crée le groupe *sftp* et l'utilisateur portal qui ne pourra pas se connecter en ssh mais seulement utiliser le sous-système *sftp*.

Pour cela nous allons l'exclure du shell grâce à `/sbin/nologin` et lui préciser son répertoire de travail `/srv` 

 * sur la vm *centos6* :

```shell
groupadd sftp
useradd -d /srv/ -s /usr/libexec/openssh/sftp-server -M -N -g sftp sftpman
passwd portal
```

`-d` spécifie le répertoire de travail
`-s` spécifie le login : Ici il ne pourra se connecter via ssh il pourra seulement utiliser le *sftp-server*
`-M` ne crée pas de /home/portal
`-N` ne crée pas de groupe portant le même nom que l'utilisateur
`-g` place l'utilisateur dans le groupe *sftp*

Nous éditons la configuration de *sshd* et ajoutons le chroot/jail: 

`/etc/ssh/sshd_config`

```console
LogLevel VERBOSE
Subsystem sftp internal-sftp

### Le jail se fait pour tout le groupe sftp et on jail dans /srv car les droits de la racine du jail doivent être root:root 755 
### On agira sur les droits de /srv/data avec setfacl et le groupe lighttpd

Match Group sftp
  X11Forwarding no
  AllowTcpForwarding no
  ChrootDirectory /srv
  ForceCommand internal-sftp -l VERBOSE
  PasswordAuthentication yes
```

Nous précisons que nous voulons du log `VERBOSE` pour le service *internal-sftp* car sans cela on ne parvient pas à bien savoir ce qui se passe sur le serveur en analysant le `/var/log/secure`.

Nous ajoutons `PasswordAuthentication yes` de telle sorte à ce que l'on puisse laisser *sshman* utilisateur classique se connecter de façon plus sécurisée par paire de clés.

Cependant nous pourrions et devrions attribuer une paire de clés à l'utilisateur portal pour une plus grande sécurité mais ça n'est pas l'objet du TP.

Afin d’éviter de rencontrer les erreurs de type : 

`fatal: bad ownership or modes for chroot directory '/srv/data'`
`pam_unix(sshd:session): session closed for user portal`

Ou encore 



Nous paramétrons les droits correctement car comme le dit le manuel de *sshd* :

>  All components of the pathname must be root-owned directories that are not writable by any other user or group. After the chroot, sshd(8) changes the working directory to the user's home directory.

```console
chown -R root:root /srv 
chmod -R go-w /srv
```


Maintenant coté client nous testons le *sftp*. C'est ici que *tmux* peut être d'une grande aide en loguant dans une autre fenêtre.


Pour éviter également les erreur d’accès sftp de type :

`localhost sshd[3486]: sent status Permission denied`


Nous préparons le changement automatique des droits pour les futurs fichiers/répertoires au sein de */srv/data/* avec `setfactl`

* Sur la vm centos6:

```shell
setfacl -Rdm g:sftp:rx /srv/data/
```

Nous consultons les ACL du dossier */srv/data/*

```shell
ls -lah /srv/data/

total 24K
drwxr-xr-x+ 3 lighttpd sftp 4,0K  3 déc.  00:57 .
drwxr-xr-x. 3 root     root 4,0K  3 déc.  00:46 ..
drwxrwxr-x+ 2 lighttpd sftp 4,0K  3 déc.  01:20 images
-rw-rw-r--. 1 lighttpd sftp  516  3 déc.  01:22 index.html
```



<a name="part4">

### 4. Test de nos services :

</a>

Imaginons que nous n'avons pas accès au *ssh* de la baremetal et que seul le bond sur la *vmcentos6* soit possible. Comment tester nos services ?

Il faudra router notre trafic via ssh directement vers notre client final : Le laptop. Cela signifie deux bonds. 

__centos6vm:443---:22-->baremetal:22------>:22------laptop(centos):443__

Tunelling ssh : 

* Gardez à l'esprit que le tunnelling suppose que vos deux serveurs ssh sont correctement configurés.

Par ailleurs sans accès ssh au baremetal c''est plus compliqué

Lorsque le tunnel passera tout le trafic sur localhost:667 du laptop sera en fait le trafic de 192.168.122:443.

Voici un exemple de commande pour deux bonds.

* Sur le baremetal : 

```shell
ssh -N -L 666:localhost:666 testman@192.168.122.20
```

* Sur le laptop

```shell
ssh -N -L 666:localhost:667 user1@192.168.1.100
```

Et voila !

Test via curl

```shell
curl -kI https://localhost

HTTP/1.1 200 OK
Date: Tue, 03 Dec 2019 13:21:02 GMT
Server: lighttpd/1.4.47
Content-Type: application/octet-stream
Accept-Ranges: bytes
ETag: "788099924"
Last-Modified: Tue, 03 Dec 2019 10:25:11 GMT
Connection: close
```


Nous nous loguons avec l'utilisateur *portal* utiliserons aussi le debug intégré à *lftp* .


* Sur le laptop:

```shell
lftp sftp://portal:portal@localhost/data
cd ok, cwd=/data                                       
lftp portal@192.168.122.20:/data> ls
drwxr-xr-x    3 495      501          4096 Dec  2 23:57 .
drwxr-xr-x    3 0        0            4096 Dec  2 23:46 ..
drwxrwxr-x    2 495      501          4096 Dec  3 00:20 images
-rw-rw-r--    1 495      501           516 Dec  3 00:22 index.html
```

Maintenant nous tentons un put dans *lftp* :

* Sur le laptop :


```shell
lftp portal@localhost:/data> put fichier.html
```



Afin effectuer des tests directement dans le navigateur nous réglons Firefox par exemple comme ceci : 

![proxy_ssh](/uploads/3fe2a634f265b6679472b989081faf0a/proxy_ssh.png)



<a name="part5">

### 5. Script de sauvegarde et vérification de la sauvegarde.

</a>

Nous rédigeons un script très simple et nous le commentons. Nous le plaçons dans `/usr/local/bin/www_backup.sh`

```bash
#!/bin/bash
# Script de sauvegarde hebdomadaire du serveur Web :8080  /srv/data distribué sur le proxy apache :443
# Ce script sauvegarde le contenu du serveur web dans le répertoire /backup/ sur 7 jours max.

set -e
# On exporte la Date en Français

LC_ALL=fr_Fr.utf8 date
TIME=$(date +%A)           # Cette commande ajoute au début du nom la date du jour
FILENAME="$TIME_backup.tar.gz"    # Ici nous définissons le nom final de  l archive
SRCDIR=/srv/data              # Le répertoire que l on veut sauvegarder
DESDIR=/backup            # Le répertoire dans lequel on place notre archive

# La comande tar qui va compresser nos fichiers.

tar -cpzf $DESDIR/$FILENAME $SRCDIR

# Nous supprimons les sauvegarde de plus de 7 jours.
find /backup -name "*.tar.gz" -type f -mtime +7 -exec rm -fr {} \;                                                                                                                                                 

#FIN DU SCRIPT
exit 0
```

Nous pouvons vérifier notre script sur un site comme [shellcheck](https://www.shellcheck.net/)


Nous rendons exécutable le script afin de le tester.

```shell
chmod u=rwx,go-wx /usr/local/bin/www_backup.sh 
```

Et le testons :

```shell
./usr/local/bin/www_backup.sh && ls -lah /backup/
```

Pour vérifier que notre sauvegarde fonctionne nous déployons les fichiers directement dans / 

```shell
tar -xvf /backup/lundi.tar.gz -C /
```


<a name="part6">

### 6.  Comment améliorer notre service ?

</a>

* Utiliser *Nginx* ?
* Mettre en place une politique de pare-feu avec *iptables* ou *firewalld*.
* Forcer et configurer *SELinux*.
* Mettre en place des paires de clés *RSA* pour le ssh la version de ssh trop old ne gère pas l'*ed25519*.
* Proposer le *TLS 1.3* uniquement et améliorer nos configurations *lighttpd* et *apache* (*Load Balancing*).
* Utiliser Nginx reverse proxy et *load balancing* ?!
* Compresser nos sauvegardes en *lzma* niveau 9.
* Utiliser *rsync* pour mettre ne place des sauvegardes incrémentielles.
* Utiliser *Nginx* !!!



Ressources :

https://httpd.apache.org/docs/2.2/ssl/ssl_howto.html

https://httpd.apache.org/docs/2.2/mod/mod_proxy.html#proxypass
